DELETE FROM book;
ALTER TABLE book AUTO_INCREMENT = 1001;

DELETE FROM category;
ALTER TABLE category AUTO_INCREMENT = 1001;

INSERT INTO `category` (`name`) VALUES ('Classics'),('Fantasy'),('Mystery'),('Romance');

INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('The Iliad', 'Homer', '', 699, 3, TRUE, FALSE, 1001);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('The Brothers Karamazov', 'Fyodor Dostoyevski', '', 799, 3, TRUE, FALSE, 1001);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('Little Dorrit', 'Charles Dickens', '', 599, 3, TRUE, FALSE, 1001);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('The Housemaid', 'Freida McFadden', '', 599, 3, TRUE, FALSE, 1001);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('To Kill a Mockingbird', 'Harper Lee', 'Classic novel exploring racial injustice and moral growth.', 999, 4.5, TRUE, FALSE, 1001);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('1984', 'George Orwell', 'Dystopian novel depicting a totalitarian regime.', 1099, 4.4, TRUE, TRUE, 1002);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('Pride and Prejudice', 'Jane Austen', 'Classic romantic novel set in 19th-century England.', 899, 4.6, TRUE, TRUE, 1003);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('The Great Gatsby', 'F. Scott Fitzgerald', 'Exploration of the American Dream and excess in the Roaring Twenties.', 799, 4.3, TRUE, FALSE, 1004);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('Harry Potter and the Sorcerer\'s Stone', 'J.K. Rowling', 'Fantasy novel about a young wizard.', 1199, 4.8, FALSE, TRUE, 1001);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('The Hobbit', 'J.R.R. Tolkien', 'Fantasy adventure novel involving a hobbit and a dragon.', 849, 4.7, FALSE, FALSE, 1002);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('The Catcher in the Rye', 'J.D. Salinger', 'Coming-of-age novel following a teenage boy in New York.', 699, 4.0, TRUE, FALSE, 1003);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('To the Lighthouse', 'Virginia Woolf', 'Modernist novel exploring consciousness and perception.', 799, 4.2, FALSE, TRUE, 1004);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('Brave New World', 'Aldous Huxley', 'Dystopian novel depicting a technologically advanced future society.', 899, 4.1, TRUE, TRUE, 1001);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('The Lord of the Rings', 'J.R.R. Tolkien', 'Epic fantasy trilogy following a quest to destroy a powerful ring.', 1599, 4.9, FALSE, FALSE, 1002);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('Crime and Punishment', 'Fyodor Dostoevsky', 'Psychological novel exploring the consequences of a young man\'s actions.', 849, 4.5, TRUE, FALSE, 1003);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('The Road', 'Cormac McCarthy', 'Post-apocalyptic novel about a father and son\'s journey.', 749, 4.4, FALSE, TRUE, 1004);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('Moby Dick', 'Herman Melville', 'Adventure novel centered around Captain Ahab\'s pursuit of a white whale.', 799, 4.3, FALSE, FALSE, 1001);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('The Road Not Taken', 'Robert Frost', 'Collection of renowned poems by Robert Frost.', 599, 4.6, FALSE, FALSE, 1002);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('Sapiens: A Brief History of Humankind', 'Yuval Noah Harari', 'Exploration of the history and impact of Homo sapiens.', 1299, 4.7, FALSE, FALSE, 1003);
INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id) VALUES ('Educated', 'Tara Westover', 'Memoir recounting a woman\'s journey from a rural childhood to education and beyond.', 1099, 4.8, FALSE, FALSE, 1004);

