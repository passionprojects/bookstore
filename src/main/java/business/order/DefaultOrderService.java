package business.order;

import api.ApiException;
import business.BookstoreDbException;
import business.JdbcUtils;
import business.book.Book;
import business.book.BookDao;
import business.cart.ShoppingCart;
import business.cart.ShoppingCartItem;
import business.customer.Customer;
import business.customer.CustomerDao;
import business.customer.CustomerForm;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

public class DefaultOrderService implements OrderService {

	private BookDao bookDao;
	private OrderDao orderDao;
	private CustomerDao customerDao;
	private LineItemDao lineItemDao;

	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}
	public void setOrderDao(OrderDao orderDao) {
		this.orderDao = orderDao;
	}
	public void setCustomerDao(CustomerDao customerDao) {
		this.customerDao = customerDao;
	}
	public void setLineItemDao(LineItemDao lineItemDao) {
		this.lineItemDao = lineItemDao;
	}


	@Override
	public OrderDetails getOrderDetails(long orderId) {
		Order order = orderDao.findByOrderId(orderId);
		Customer customer = customerDao.findByCustomerId(order.customerId());
		List<LineItem> lineItems = lineItemDao.findByOrderId(orderId);
		List<Book> books = lineItems
				.stream()
				.map(lineItem -> bookDao.findByBookId(lineItem.bookId()))
				.toList();
		return new OrderDetails(order, customer, lineItems, books);
	}

	@Override
    public long placeOrder(CustomerForm customerForm, ShoppingCart cart) {

		validateCustomer(customerForm);
		validateCart(cart);
		// NOTE: MORE CODE PROVIDED NEXT PROJECT
		try (Connection connection = JdbcUtils.getConnection()) {
			Date ccExpDate = getCardExpirationDate(
					customerForm.getCcExpiryMonth(),
					customerForm.getCcExpiryYear());
			return performPlaceOrderTransaction(
					customerForm.getName(),
					customerForm.getAddress(),
					customerForm.getPhone(),
					customerForm.getEmail(),
					customerForm.getCcNumber(),
					ccExpDate, cart, connection);
		} catch (SQLException e) {
			throw new BookstoreDbException("Error during close connection for customer order", e);
		}
	}

	private Date getCardExpirationDate(String monthString, String yearString) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, Integer.parseInt(monthString));
		cal.set(Calendar.YEAR, Integer.parseInt(yearString));
		Date expDate = cal.getTime();
		return expDate;
	}

	private long performPlaceOrderTransaction(
			String name, String address, String phone,
			String email, String ccNumber, Date date,
			ShoppingCart cart, Connection connection) {
		try {
			connection.setAutoCommit(false);
			long customerId = customerDao.create(
					connection, name, address, phone, email,
					ccNumber, date);
			long customerOrderId = orderDao.create(
					connection,
					cart.getComputedSubtotal() + cart.getSurcharge(),
					generateConfirmationNumber(), customerId);
			for (ShoppingCartItem item : cart.getItems()) {
				lineItemDao.create(connection, customerOrderId,
						item.getBookId(), item.getQuantity());
			}
			connection.commit();
			return customerOrderId;
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				throw new BookstoreDbException("Failed to roll back transaction", e1);
			}
			return 0;
		}
	}

	private int generateConfirmationNumber() {
		return ThreadLocalRandom.current().nextInt(999999999);
	}

	private void validateCustomer(CustomerForm customerForm) {

    	String name = customerForm.getName();
		String email = customerForm.getEmail();
		String address = customerForm.getAddress();
		String phone = customerForm.getPhone();
		String ccNumber = customerForm.getCcNumber();

		validateName(name);
		validateEmail(email);
		validateAddress(address);
		validatePhone(phone);
		validateCCNumber(ccNumber);
		validateExpiryDate(customerForm.getCcExpiryMonth(), customerForm.getCcExpiryYear());
	}
	private void validateName(String formField){
		if (formField == null || formField.length() > 45 || formField.length() < 4) {
			throw new ApiException.ValidationFailure("Invalid name field");
		};
	}

	private void validateEmail(String formEmail) {
		if (formEmail == null || formEmail.isEmpty()) {
			throw new ApiException.ValidationFailure("Email cannot be null");
		}

		String emailRegex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+[A-Za-z]$";
		if (!(Pattern.compile(emailRegex)
				.matcher(formEmail)
				.matches())) {
			throw new ApiException.ValidationFailure("Invalid email field");
		}
	}

	private void validateAddress(String formAddress){
		if (formAddress == null || formAddress.length() < 4 || formAddress.length() > 45) {
			throw new ApiException.ValidationFailure("Address cannot be null");
		}

		String addressRegex = "\\d+\\s+[A-Za-z0-9\\s]+";

		if (!Pattern.compile(addressRegex).matcher(formAddress).matches()) {
			throw new ApiException.ValidationFailure("Invalid address field");
		}
	};

	private void validatePhone(String formPhone){
		if (formPhone == null || formPhone.isEmpty()) {
			throw new ApiException.ValidationFailure("Phone cannot be null");
		}
		String phoneRegex = "^(\\+?1[\\s.-]?)?\\(?[2-9][0-9]{2}\\)?[\\s.-]?[2-9][0-9]{2}[\\s.-]?[0-9]{4}$";
		if (!(Pattern.compile(phoneRegex)
				.matcher(formPhone)
				.matches())) {
			throw new ApiException.ValidationFailure("Invalid phone field");
		};
	};

	private void validateCCNumber(String formCCNumber){
		if (formCCNumber == null || formCCNumber.isEmpty()) {
			throw new ApiException.ValidationFailure("cc number cannot be null");
		}
		String sanitized = formCCNumber.replaceAll("[- ]+", "");
		String creditCardRegex = "^(?:4[0-9]{12}(?:[0-9]{3,6})?|5[1-5][0-9]{14}|(222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|6(?:011|5[0-9][0-9])[0-9]{12,15}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11}|6[27][0-9]{14}|^(81[0-9]{14,17}))$";

		if (!sanitized.matches(creditCardRegex)) {
			throw new ApiException.ValidationFailure("Invalid credit card text date");
		}

		int sum = 0;
		int digit;
		int tmpNum;
		boolean shouldDouble = false;

		for (int i = sanitized.length() - 1; i >= 0; i--) {
			digit = Character.getNumericValue(sanitized.charAt(i));
			tmpNum = digit;

			if (shouldDouble) {
				tmpNum *= 2;

				if (tmpNum >= 10) {
					sum += (tmpNum % 10) + 1;
				} else {
					sum += tmpNum;
				}
			} else {
				sum += tmpNum;
			}

			shouldDouble = !shouldDouble;
		}

		if (!(sum % 10 == 0)) {
			throw new ApiException.ValidationFailure("Invalid credit card number date");

		};
	};
	private void validateExpiryDate(String ccExpiryMonth, String ccExpiryYear) {
		if (ccExpiryMonth == null || ccExpiryYear == null || ccExpiryYear.isEmpty() || ccExpiryMonth.isEmpty()) {
			throw new ApiException.ValidationFailure("expiration cannot be null");
		}
		try {
			int month = Integer.parseInt(ccExpiryMonth);
			int year = Integer.parseInt(ccExpiryYear);
			YearMonth formDate = YearMonth.of(year, month);
			if (formDate.isBefore(YearMonth.now())) {
				throw new ApiException.ValidationFailure("Invalid expiry date");
			}
		}
		catch (Exception e) {
			throw new ApiException.ValidationFailure(e.getMessage());
		}
	}

	private void validateCart(ShoppingCart cart) {
		validCartSize(cart.getItems().size());
		validBookItems(cart.getItems());
	}

	private void validCartSize(int cartSize) {
		if (cartSize < 1){
			throw new ApiException.ValidationFailure("Cart size is invalid.");
		}
	}

	private void validBookItems(List<ShoppingCartItem> cartItems){
			for (ShoppingCartItem item : cartItems) {
				Book databaseBook = bookDao.findByBookId(item.getBookId());
				long dbBookCategory = databaseBook.categoryId();
				int dbBookPrice = databaseBook.price();

				if (item.getQuantity() < 0 || item.getQuantity() > 99) {
					throw new ApiException.ValidationFailure("Invalid quantity");
				}
				if (dbBookPrice != item.getBookForm().getPrice()) {
					throw new ApiException.ValidationFailure("Invalid book price");
				}
				if (dbBookCategory != item.getBookForm().getCategoryId()) {
					throw new ApiException.ValidationFailure("Invalid book category");
				}
			}

	}


}
